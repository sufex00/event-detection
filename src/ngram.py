# -*- coding: utf-8 -*-
"""
Created on Tue Oct 24 21:48:43 2017

@author: pedro
"""

import re


emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""

regex_str = [
    emoticons_str,
    r'<[^>]+>', # HTML tags
    r'(?:@[\w_]+)', # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs

    r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
    r"(?:[0-9][0-9'\-_]+[0Frequency -9])",
    r'(?:[\w_]+)' # other words
# anything else
]


tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)

from nltk.corpus import stopwords
import string

punctuation = list(string.punctuation)
stop = stopwords.words('english') + punctuation + ['rt', 'via', '\\', 'http']

def tokenize(s):
    return tokens_re.findall(s)

def preprocess(s,lowercase=False):
    tokens = tokenize(s)
    if lowercase:
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens

tweet = 'RT @marcobonzanini: just an example! :D http://example.com #NLP'
print(preprocess(tweet, 'just'))

from itertools import tee, islice

def ngrams(lst, n):
  tlst = lst
  while True:
    a, b = tee(tlst)
    l = tuple(islice(a, n))
    if len(l) == n:
      yield l
      next(b)
      tlst = b
    else:
      break

import csv

from os import listdir
from os.path import isfile, join
from collections import Counter
import tqdm
from tqdm import *
import pandas

import scipy as sc
import numpy as np
from scipy.stats import linregress
mypath = '../db/FA_Cup/'

cut = 8

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()

def count(low, high, n):

    count_all = Counter()
    for e in onlyfiles:
        with open(mypath+e, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                terms_only = [term for term in preprocess(row['first_name'], True)
                      if term not in stop and
                      not term.startswith(('#', '@', '\\', 'https', 'http'))
                      and row['last_name'][:-11] > str(low[3]+' '+get_moth(low[1])+' '+str(low[2])+' '+low[4]+' +0000 '+str(low[0]))[:-11] and
                                                row['last_name'][:-11] < str(high[3]+' '+get_moth(high[1])+' '+str(high[2])+' '+high[4]+' +0000 '+str(high[0]))[:-11]]

                count_all.update(ngrams(terms_only, n))
    return count_all
def series_word(word = 'all_words',bias_time = '1Min'):
    dates_find_word = []
    dates_words = []
    mypath = '../db/FA_Cup/'
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    onlyfiles.sort()
    for e in onlyfiles:
        with open(mypath+e, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                    terms_only = [term for term in preprocess(row['first_name'], True)
                    if term not in stop and
                    not term.startswith(('#', '@', '\\','https','http'))]

                    if word in terms_only:
                            if word != 'all_words':
                                dates_find_word.append(row['last_name'])
                    if word in'all_words':
                        dates_words.append(row['last_name'])

    if word in'all_words':
        ones_words = [1]*len(dates_words)
        idx_words = pandas.DatetimeIndex(dates_words)
        words = pandas.Series(ones_words, index=idx_words)
        per_minute_words = words.resample(bias_time).sum()#, how='sum').fillna(0)
        return words, per_minute_words
    else:
        ones_find = [1]*len(dates_find_word)
        idx_find = pandas.DatetimeIndex(dates_find_word)
        find_word = pandas.Series(ones_find, index=idx_find)
        per_minute_find = find_word.resample(bias_time).sum()#, how='sum').fillna(0)
        return find_word, per_minute_find

s, p = series_word(bias_time='1Min')
print('Data                       |  Frequence')
print(p)



def add_vector(current, current_t,end, next_t, dir_t = '../db/hash_count/'):
    s = {}
    try:
        with open(dir_t+str(current)+current_t[8:]+'-'+str(end)+next_t[8:]+'.csv', 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                s[row['first']+'-'+row['second']] = int(row['freq'])
    except :
        s = 'fail'
    return s


def calculate(start, end, s):
    map_t = dict(s)
    for idx in range(len(p)):
        if str(p[idx:idx+1].index[0])[cut:] >= start and str(p[idx:idx+1].index[0])[cut:] < end:
            start_t = str(p[idx:idx+1].index[0])
            end_t = str(p[idx+1:idx+2].index[0])

            ts = data(start_t)
            te = data(end_t)

            aux_map = add_vector(ts, start_t, te, end_t)
            if aux_map != 'fail':
                for key in aux_map:
                    try:
                        map_t[key] = map_t[key] + aux_map[key]
                    except:
                        map_t[key] = aux_map[key]
    return map_t

def calculate_prob(current, sum_t):
    return_map = dict(current)
    for key in current:
        return_map[key] = float(current[key])/sum_t[key] #* current[key]
    return return_map

def create_list_prob(start, end, ngram, s):
    prob_list = []
    for idx in range(len(p)):
        if str(p[idx:idx+1].index[0])[cut:] >= start and str(p[idx:idx+1].index[0])[cut:] < end:
            start_t = str(p[idx:idx + 1].index[0])
            end_t = str(p[idx + 1:idx + 2].index[0])

            ts = data(start_t)
            te = data(end_t)

            aux_map = add_vector(ts, start_t, te, end_t)
            if aux_map != 'fail':
                try:
                    prob_map = calculate_prob(aux_map, s)
                    prob_list.append(prob_map[ngram])
                except:
                    prob_list.append(0.0)
    return prob_list

def string_separate(str_t):
    a = 0
    for idx, e in enumerate(str_t):
        if e == '-':
            a = idx
    return str_t[ : a], str_t[a+1 : ]

def salve_event(aux_set):
    find = True
    for s in aux_set:
        if key_b[0] in s or key_b[1] in s:
            s.add(key_b[0])
            s.add(key_b[1])
            find = False
            break
    if len(aux_set) == 0 or find:
        s = set()
        s.add(key_b[0])
        s.add(key_b[1])
        aux_set.append(s)

    return aux_set

def agrupate(aux_set):
    disju = []
    for s in range(len(aux_set)):
        for i in range(s+1, len(aux_set)):
            if not aux_set[s].isdisjoint(aux_set[i]):
                disju.append((s, i))

    for t in disju:
        for e in aux_set[t[1]]:
            aux_set[t[0]].add(e)
    disju = [t[1] for t in disju]
    aux_set_new = [e for index, e in enumerate(aux_set) if index not in disju ]
    return aux_set_new


def print_cluster(cluster, labels):
#    print('set = ', len(cluster))
    event_num = 0
    for i, e in enumerate(cluster):
        aux = []
        for c in e:
            v = list(labels.keys())
            aux.append(v[c])
        if len(aux) > 2:
            event_num += 1
            print('Event #', event_num, '(', len(aux), ')')
            print(aux)


def normalizeMatrix(m):
    m = m.astype('float32')
    for e in range(len(m)):
        sum = m[:,e].sum()
        m[:,e]= m[:,e].dot(1/sum)
    return m

def inflateMatrix(m, r):
    m = m.astype('float32')
    m[:,:] = m[:, :] ** r
    m = normalizeMatrix(m)
    return m

def powerMatrix(m, p):
    m = m.astype('float32')
    for e in range(1,p):
        m = m.dot(m)
    return m

def verify (m, r):
    very = m.round(3) == inflateMatrix(m, r).round(3)
    flag = True
    for e in range(len(very)):
        for i in range(len(very)):
            if very[e][i] == False:
                flag = False
                break
    return not flag

def get_cluster(m):
    cluster = set()
    b = -1
    for e in range(len(m)):
        aux = []
        for index, i in enumerate(m[e,:]):
            if i != 0:
                aux.append(index)
        if len(aux)!= 0:
            cluster.add(tuple(aux))
        if len(aux)>1:
            b = 0
    return cluster, b

def MCL(m, r, p):
    m = normalizeMatrix(m)
    m = powerMatrix(m, p)
    m = inflateMatrix(m, r)
    while(verify(m, r)):
        m = powerMatrix(m, p)
        m = inflateMatrix(m, r)
    return get_cluster(m.round(3))

def constIndexMatriz(g):
    index = {}
    for e in g:
        if e[0] not in index:
            index[e[0]]=len(index)
        if e[1] not in index:
            index[e[1]]=len(index)
    return index

def constMatrix(g, index):
    m = np.zeros((len(index), len(index)))
    for e in g:
        m[index[e[0]]][index[e[1]]] = e[2]
        m[index[e[1]]][index[e[0]]] = e[2]
    return m

def tup_series(tup):
    n=1
    tseries = []
    for magnitude in tup:
        tseries.append((n,magnitude))
        n=n+1
    return tseries

def get_twitter_time( discrete_time):

    global onlyfiles
    terms_only = {}
    for e in tqdm(onlyfiles):
        with open(mypath+e, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                for e in range(len(discrete_time)-1):
                    if discrete_time[e] <= row['last_name'][8:-11] < discrete_time[e+1]:
                        try:
                            terms_only[discrete_time[e]].append([term for term in preprocess(row['first_name'], True)
                            if term not in stop and
                            not term.startswith(('#', '@', '\\', 'https', 'http'))])
                        except:
                            terms_only[discrete_time[e]] = []
                            terms_only[discrete_time[e]].append([term for term in preprocess(row['first_name'], True)
                                                              if term not in stop and
                                                              not term.startswith(('#', '@', '\\', 'https', 'http'))])

    return terms_only

def Levenshtein_coorelation(s, minsup):
    s_map = []
    result = []
    for itemset, support in find_frequent_itemsets(s, minsup, True):
        result.append((itemset,support))

    result = sorted(result, key=lambda i: i[0])
    for e in result:
        try:
            if len(e[0]) == 2:
                s_map.append((e[0][0],e[0][1],1 ))
        except:
            s_map = 'fail'
    return s_map

import nltk

def dist_words(a,b):
    aa = nltk.edit_distance(a[0], b[0])
    bb = nltk.edit_distance(a[0], b[1])

    ab = nltk.edit_distance(a[0], b[1])
    ba = nltk.edit_distance(a[1], b[0])

    return min(aa+bb, ab+ba)


def data(time):
    import datetime
    year = int(time[:4])
    mon = int(time[5:7])
    day = time[8:10]
    h = time[11:]
    day_t = datetime.datetime(year, mon, int(day)).weekday()
    return (year, mon, day, get_week(day_t), h)


def get_week(weekday):
    if weekday == 0:
        return 'Mon'
    if weekday == 1:
        return 'Tue'
    if weekday == 2:
        return 'Wed'
    if weekday == 3:
        return 'Thu'
    if weekday == 4:
        return 'Fri'
    if weekday == 5:
        return 'Sat'
    if weekday == 6:
        return 'Sun'


def get_moth(m):
    if m == 1:
        return 'Jan'
    if m == 2:
        return 'Feb'
    if m == 3:
        return 'Mar'
    if m == 4:
        return 'Apr'
    if m == 5:
        return 'May'
    if m == 6:
        return 'Jun'
    if m == 7:
        return 'Jul'
    if m == 8:
        return 'Aug'
    if m == 9:
        return 'Set'
    if m == 10:
        return 'Oct'
    if m == 11:
        return 'Nov'
    if m == 12:
        return 'Dec'

print('# of tweets: ', len(s))



bag = 10
next_v = bag
epson = 1.0e-030
n_gran = 2
minsup = 4


s = []
pedro = []
pedro_x = []

discrete_time = [str(p[e:e+1].index[0])[8:] for e in range(len(p)-1)]

s = get_twitter_time(discrete_time)

normalize = np.log(bag)

flag=0
b = 0


for idx, e in enumerate(p):

    if idx>bag and idx+bag < len(p):
        x = p[idx:idx+next_v]  
        x = np.mean(x)

        x = p[idx]

        time_start = str(p[idx:idx+1].index[0])[cut:]
        time_end = str(p[idx+next_v-1:idx+next_v].index[0])[cut:]
        time_current =  str(p[idx+next_v:idx+next_v+1].index[0])[cut:]

        tc = data(str(p[idx+next_v:idx+next_v+1].index[0]))
        te = data(str(p[idx+next_v-1:idx+next_v].index[0]))
        ts = data(str(p[idx:idx+1].index[0]))
        print('Data : ', data(str(p[idx+next_v-2:idx+next_v-1].index[0])))

        t = add_vector(te, str(p[idx+next_v-1:idx+next_v].index[0]),tc, str(p[idx+next_v:idx+next_v+1].index[0])) 

        if t != 'fail':
            graph = []
            a = calculate(time_start, time_end, t)
            calculate_prob(t, a)
            aux_set = []
            for key in t:
#
                    t[key] = create_list_prob(time_start, time_current,key, a)
                    max_v  = np.max(t[key])
                    aux = t[key][-1]
                    ent = sc.stats.entropy(t[key])
                    pedro_x.append(time_end)

                    if aux == max_v  and ent >= 0.1 and ent <= 0.7 and a[key] >= 15:
                        flag = True
                        key_b = key.split('-')

                        try:
                            key_list = Levenshtein_coorelation(s[time_end], minsup)

                        except:
                            key_list = []

                        graf_key = []

                        for element in key_list:
                            if dist_words(element, key_b) < 3:
                                graf_key.append(element)
                                graph.append(element)

                        graph.append((key_b[0], key_b[1], 1))

            #            print('keywords:',key_b,'time:', time_end, key,'value:',a[key], ' entropy:', ent)
            index = constIndexMatriz(graph)
            m = constMatrix(graph, index)
            cluster, b_cluster = MCL(m, 1.5, 2)
            print_cluster(cluster, index)
            if flag :
                b = b + 1
            flag = False




        print('-------------------------------------------')


