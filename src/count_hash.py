# -*- coding: utf-8 -*-
"""
Created on Tue Oct 24 21:48:43 2017

@author: pedro
"""

import re

emoticons_str = r"""
    (?:
        [:=;] # Eyes
        [oO\-]? # Nose (optional)
        [D\)\]\(\]/\\OpP] # Mouth
    )"""

regex_str = [
    emoticons_str,
    r'<[^>]+>',  # HTML tags
    r'(?:@[\w_]+)',  # @-mentions
    r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)",  # hash-tags
    r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+',  # URLs

    r'(?:(?:\d+,?)+(?:\.?\d+)?)',  # numbers
    r"(?:[a-z][a-z'\-_]+[a-z])",  # words with - and '
    r"(?:[0-9][0-9'\-_]+[0-9])",
    r'(?:[\w_]+)'  # other words
    # anything else
]

tokens_re = re.compile(r'(' + '|'.join(regex_str) + ')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^' + emoticons_str + '$', re.VERBOSE | re.IGNORECASE)

from nltk.corpus import stopwords
import string

punctuation = list(string.punctuation)
stop = stopwords.words('english') + punctuation + ['rt', 'via', '\\', 'http']


def tokenize(s) :
    return tokens_re.findall(s)


def preprocess(s, lowercase=False) :
    tokens = tokenize(s)
    if lowercase :
        tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
    return tokens


tweet = 'RT @p_pedro: just an example! :D http://example.com #NLP'
print(preprocess(tweet, 'just'))

from itertools import tee, islice


def ngrams(lst, n) :
    tlst = lst
    while True :
        a, b = tee(tlst)
        l = tuple(islice(a, n))
        if len(l) == n :
            yield l
            next(b)
            tlst = b
        else :
            break


import csv

from os import listdir
from os.path import isfile, join
from collections import Counter
from tqdm import *
import pandas

mypath = '../db/FA_Cup/'

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()


def count(low, high, n) :
    count_all = Counter()
    for e in onlyfiles :
        with open(mypath + e, 'r') as f :
            reader = csv.DictReader(f)
            for row in reader :
                terms_only = [term for term in preprocess(row['first_name'], True)
                              if term not in stop and
                              not term.startswith(('#', '@', '\\', 'https', 'http'))
                              and row['last_name'][:-11] > str(
                        low[3] + ' ' + get_moth(low[1]) + ' ' + str(low[2]) + ' ' + low[4] + ' +0000 ' + str(low[0]))[
                                                           :-11] and
                              row['last_name'][:-11] < str(
                        high[3] + ' ' + get_moth(high[1]) + ' ' + str(high[2]) + ' ' + high[4] + ' +0000 ' + str(
                            high[0]))[:-11]]
                count_all.update(ngrams(terms_only, n))
    return count_all


def series_word(word='all_words', bias_time='1Min') :
    dates_find_word = []
    dates_words = []
    mypath = '../db/FA_Cup/'
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
    onlyfiles.sort()
    for e in onlyfiles :
        with open(mypath + e, 'r') as f :
            reader = csv.DictReader(f)
            for row in reader :
                terms_only = [term for term in preprocess(row['first_name'], True)
                              if term not in stop and
                              not term.startswith(('#', '@', '\\', 'https', 'http'))]

                if word in terms_only :
                    if len(word) > 2 :
                        if word != 'all_words' :
                            dates_find_word.append(row['last_name'])
                if word in 'all_words' :
                    dates_words.append(row['last_name'])

    if word in 'all_words' :
        ones_words = [1] * len(dates_words)
        idx_words = pandas.DatetimeIndex(dates_words)
        words = pandas.Series(ones_words, index=idx_words)
        per_minute_words = words.resample(bias_time).sum()  # , how='sum').fillna(0)
        return words, per_minute_words
    else :
        ones_find = [1] * len(dates_find_word)
        idx_find = pandas.DatetimeIndex(dates_find_word)
        find_word = pandas.Series(ones_find, index=idx_find)
        per_minute_find = find_word.resample(bias_time).sum()  # , how='sum').fillna(0)
        return find_word, per_minute_find


s, p = series_word(bias_time='1Min')


def salve(start, time_start, end, time_end, list_t, dir_t='../db/hash_count/') :
    with open(dir_t + str(start) + time_start[8 :] + '-' + str(end) + time_end[8 :] + '.csv', 'w') as csvfile :
        fieldnames = ['first', 'second', 'freq']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for e in list_t :
            writer.writerow({'first' : e[0][0], 'second' : e[0][1], 'freq' : e[1]})


def data(time) :
    import datetime
    year = int(time[:4])
    mon = int(time[5 :7])
    day = time[8 :10]
    h = time[11 :]
    day_t = datetime.datetime(year, mon, int(day)).weekday()
    return (year, mon, day, get_week(day_t), h)


def get_week(weekday) :
    if weekday == 0 :
        return 'Mon'
    if weekday == 1 :
        return 'Tue'
    if weekday == 2 :
        return 'Wed'
    if weekday == 3 :
        return 'Thu'
    if weekday == 4 :
        return 'Fri'
    if weekday == 5 :
        return 'Sat'
    if weekday == 6 :
        return 'Sun'


def get_moth(m) :
    if m == 1 :
        return 'Jan'
    if m == 2 :
        return 'Feb'
    if m == 3 :
        return 'Mar'
    if m == 4 :
        return 'Apr'
    if m == 5 :
        return 'May'
    if m == 6 :
        return 'Jun'
    if m == 7 :
        return 'Jul'
    if m == 8 :
        return 'Aug'
    if m == 9 :
        return 'Set'
    if m == 10 :
        return 'Oct'
    if m == 11 :
        return 'Nov'
    if m == 12 :
        return 'Dec'


next_v = 1
n_gran = 2

s = {}

for idx in tqdm(range(len(p) - 1)) :

    x = p[idx]

    time_start = str(p[idx :idx + 1].index[0])
    time_end = str(p[idx + next_v :idx + next_v + 1].index[0])
    print('-------------------------------------------')

    ts = data(time_start)
    te = data(time_end)

    print('# tweets:', x, '- time_start : ', time_start, ' - time_end: ', time_end)

    aux = count(ts, te, n_gran)
    if len(aux) >= 400 :
        list_t = aux.most_common(400)
    else :
        list_t = aux.most_common(len(aux) - 1)
    print(len(aux))
    salve(ts, time_start, te, time_end, list_t)

    print('-------------------------------------------')
